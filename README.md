# Aprovador - Exemplo de integração via banco de dados SQL #

Este repositório contém os scripts de exemplo para uma integração do Aprovador com banco de dados [PostgreSQL](https://www.postgresql.org/download/), utilizando apenas a linguagem SQL. A documentação deste exemplo pode ser encontrada em [Aprovador - Exemplo de integração SQL](https://aprovador.com/doc/integracoes/db/exemplo).

# Para quem é este repositório ###

Este repositório foi criado para os desenvolvedores que estão interessados em integrar aplicações com o Aprovador utilizando apenas scripts SQL.


# Aprovador

Para saber mais sobre o Aprovador, acesse [https://aprovador.com](https://aprovador.com). Para saber como customizar o Aprovador, acesse o [Portal do Desenvolvedor](https://aprovador.com/documentacao/).

# Licença

Os fontes disponíveis neste repositório são licenciados sob a [Licença MIT](https://opensource.org/licenses/MIT).



