SELECT p.id                                       AS document_uid
     , 'PDF do Pedido'                            AS description
     , CONCAT('c:/anexos/pc_', pd.numero, '.pdf') AS path
FROM pendencias p
INNER JOIN pedidos pd
    ON pd.id = p.id_pedido
WHERE p.status = 'P'
  AND p.data_geracao > current_date - interval '30' day;
