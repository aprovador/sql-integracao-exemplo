SELECT p.id            AS document_uid
     , pdit.id_produto AS id
     , prod.codigo     AS text_id
     , prod.descricao  AS text_name
     , 'items'         AS collection_id
FROM pendencias p
INNER JOIN pedidos pd
    ON pd.id = p.id_pedido
INNER JOIN pedidos_itens pdit
    ON pdit.id_pedido = pd.id
INNER JOIN produtos prod
    ON prod.id = pdit.id_produto
WHERE p.status = 'P'
  AND p.data_geracao > current_date - interval '30' day;