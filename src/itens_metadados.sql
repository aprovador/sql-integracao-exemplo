SELECT p.id                                                 AS document_uid
     , prod.id                                              AS item_id 
     , cast(pdit.quantidade as varchar)                     AS pedit_quantidade
     , to_char(pdit.preco, 'L999G999D99')                   AS pedit_preco
     , pdit.un                                              AS pedit_un
     , pdit.ccusto                                          AS pedit_ccusto
     , prod.descricao                                       AS prod_descricao
     , to_char(pdit.quantidade * pdit.preco, 'L999G999D99') AS pedit_total
FROM pendencias p
INNER JOIN pedidos pd
    ON pd.id = p.id_pedido
INNER JOIN pedidos_itens pdit
    ON pdit.id_pedido = pd.id
INNER JOIN produtos prod
    ON prod.id = pdit.id_produto
WHERE p.status = 'P'
  AND p.data_geracao > current_date - interval '30' day;