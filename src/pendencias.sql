SELECT
      pend.id                          AS uid
    , pd.cod_empresa                   AS company_id
    , pd.nome_empresa                  AS company_name
    , pend.id                          AS document_id
    , 'ESP_PC'                         AS type
    , pd.data_emissao                  AS creation_date
    , pd.usuario                       AS text_from
    , 'Pedido de Compra'               AS text_subject
    , pd.observacao                    AS text_message
    , to_char(pd.valor, 'L999G999D99') AS text_value
    , forn.nome                        AS text_party
    , cp.descricao                     AS text_payment_terms
    , pd.numero                        AS text_id
    , '-okeot1bZ'                      AS layout_id
FROM pendencias pend
INNER JOIN pedidos pd
    ON pd.id = pend.id_pedido
INNER JOIN fornecedores forn
    ON forn.id = pd.id_fornecedor
INNER JOIN cond_pagtos cp
    ON cp.id = pd.id_cond_pagto
WHERE pend.status       = 'P'
  AND pend.data_geracao > current_date - interval '30' day;