SELECT p.id                                   AS document_uid
     , pd.numero                              AS pedido_numero
     , pd.observacao                          AS pedido_observacao
     , to_char(pd.valor, 'L999G999D99')       AS pedido_valor
     , to_char(pd.data_emissao, 'DD/MM/YYYY') AS pedido_emissao
     , f.id                                   AS fornecedor_id
     , f.nome                                 AS fornecedor_nome
     , f.cnpj                                 AS fornecedor_cnpj
     , cp.descricao                           AS condpag_descricao
FROM pendencias p
INNER JOIN pedidos pd
    ON pd.id = p.id_pedido
INNER JOIN fornecedores f
    ON f.id = pd.id_fornecedor
LEFT JOIN cond_pagtos cp
    ON cp.id = pd.id_cond_pagto
WHERE p.status = 'P'
  AND p.data_geracao > current_date - interval '30' day;