DO
$do$
BEGIN

IF (SELECT 1 FROM pendencias pend INNER JOIN pedidos ped ON ped.id = pend.id_pedido AND ped.valor > 10000 
        WHERE pend.id = :documentId) = 1 THEN
    RAISE EXCEPTION 'Usuario :aprovadorUser nao pode aprovar pedidos com valor maior do que R$ 10.000,00.';
ELSE
    IF ':statusCode' = 'approved' THEN
        UPDATE pendencias SET status = 'A' WHERE ID = :documentId;
    ELSE
        UPDATE pendencias SET status = 'R' WHERE ID = :documentId;
    END IF;

    UPDATE pendencias SET observacao = ':statusDescription' WHERE ID = :documentId;
END IF;

END
$do$