SELECT p.id     AS document_uid
     , a.email  AS aprovador_user
FROM pendencias p
INNER JOIN aprovadores a
    ON a.aprova_pc = true
WHERE p.status = 'P'
  AND p.data_geracao > current_date - interval '30' day;