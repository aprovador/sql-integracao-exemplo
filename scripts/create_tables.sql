--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: aprovadores; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aprovadores (
    id serial,
    nome character varying,
    email character varying,
    aprova_pc boolean
);


ALTER TABLE public.aprovadores OWNER TO postgres;

--
-- Name: cond_pagtos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cond_pagtos (
    id serial,
    descricao character varying
);


ALTER TABLE public.cond_pagtos OWNER TO postgres;

--
-- Name: fornecedores; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fornecedores (
    id serial,
    nome character varying,
    cnpj character varying,
    telefone character varying
);


ALTER TABLE public.fornecedores OWNER TO postgres;

--
-- Name: pedidos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pedidos (
    id serial,
    numero character varying,
    valor double precision,
    cod_empresa character varying,
    nome_empresa character varying,
    data_emissao date,
    usuario character varying,
    observacao text,
    id_fornecedor integer,
    id_cond_pagto integer
);


ALTER TABLE public.pedidos OWNER TO postgres;

--
-- Name: pedidos_itens; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pedidos_itens (
    id serial,
    id_pedido integer,
    id_produto integer,
    quantidade integer,
    un character varying,
    preco double precision,
    ccusto character varying
);


ALTER TABLE public.pedidos_itens OWNER TO postgres;

--
-- Name: pendencias; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pendencias (
    id serial,
    id_pedido integer,
    status character varying,
    observacao text,
    data_geracao date,
    id_aprovador integer
);


ALTER TABLE public.pendencias OWNER TO postgres;

--
-- Name: produtos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.produtos (
    id serial,
    descricao character varying,
    un character varying,
    codigo character varying
);


ALTER TABLE public.produtos OWNER TO postgres;

--
-- Name: pedidos pedido_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pedidos
    ADD CONSTRAINT pedido_pkey PRIMARY KEY (id);


--
-- Name: pedidos_itens pedidos_itens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pedidos_itens
    ADD CONSTRAINT pedidos_itens_pkey PRIMARY KEY (id);


--
-- Name: pendencias pendencias_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pendencias
    ADD CONSTRAINT pendencias_pkey PRIMARY KEY (id);


--
-- Name: produtos produtos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.produtos
    ADD CONSTRAINT produtos_pkey PRIMARY KEY (id);
    
    
--
-- Name: aprovadores aprovadores_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aprovadores
    ADD CONSTRAINT aprovadores_pkey PRIMARY KEY (id);
    
    
--
-- Name: cond_pagtos cond_pagtos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cond_pagtos
    ADD CONSTRAINT cond_pagtos_pkey PRIMARY KEY (id);


--
-- Name: fornecedores fornecedores_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fornecedores
    ADD CONSTRAINT fornecedores_pkey PRIMARY KEY (id); 

--
-- PostgreSQL database dump complete
--

