INSERT INTO public.pedidos (
	numero, valor, cod_empresa, nome_empresa, data_emissao, usuario, observacao, id_fornecedor, id_cond_pagto)
	VALUES('000001', 8000, '10', 'Empresa Exemplo', CURRENT_DATE, 'Alan Turing', 'Compra para teste de aprovacao', 1, 1);

INSERT INTO public.pedidos_itens (
	id_pedido, id_produto, quantidade, un, preco, ccusto)
SELECT id, 1, 1, 'un', 5000, 'TI'
FROM public.pedidos ORDER BY id DESC LIMIT 1;

INSERT INTO public.pedidos_itens (
	id_pedido, id_produto, quantidade, un, preco, ccusto)
SELECT id, 2, 2, 'un', 1500, 'ADM'
FROM public.pedidos ORDER BY id DESC LIMIT 1;

INSERT INTO public.pendencias (
	id_pedido, status, data_geracao, id_aprovador)
SELECT id, 'P', CURRENT_DATE, 1
FROM public.pedidos ORDER BY id DESC LIMIT 1;

-- DELETE FROM pedidos;
-- DELETE FROM pedidos_itens;
-- DELETE FROM pendencias;
