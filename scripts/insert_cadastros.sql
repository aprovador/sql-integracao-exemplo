-- Inclua aprovadores com e-mails cadastrados para a sua empresa no Aprovador.
INSERT INTO public.aprovadores(
	id, nome, email, aprova_pc)
	VALUES (1, 'Usuário de exemplo', 'usuario+db@aprovador.com', true);
	
INSERT INTO public.cond_pagtos(
	id, descricao)
	VALUES
		(1, 'A vista'),
		(2, '30 dias'),
		(3, '30/60 dias');
		
INSERT INTO public.fornecedores(
	id, nome, cnpj, telefone)
	VALUES
		(1, 'Apple Computers SA', '44.676.410/0001-22', '(11) 2566-0099'),
		(2, 'Mercado Livre SA', '12.345.678/9101-12', '(11) 5500-9900');
		
INSERT INTO public.produtos(
	id, descricao, un, codigo)
	VALUES
		(1, 'Computador', 'un', 'DESKTOP'),
		(2, 'Android', 'un', 'ANDROID'),
		(3, 'iPhone', 'un', 'IPHONE');

-- DELETE FROM public.aprovadores;
-- DELETE FROM public.cond_pagtos;
-- DELETE FROM public.fornecedores;
-- DELETE FROM public.produtos;

